;;; init.el --- Init Emacs file

;;; Author: Javier de Marco <javierdemarcoo@gmail.com>
;;; URL: https://gitlab.com/javierdemarco/emacs
;;; Version: 0.0.1


;;; Commentary:
;;; Init Emacs file

;;; Code:

;; ====================================
;; Basic Customization
;; ====================================
(load "~/.emacs.d/configs/base.el")
;; ====================================
;; General
;; ====================================
(load "~/.emacs.d/configs/package-general.el")
;; ====================================
;; Keybindings
;; ====================================
(load "~/.emacs.d/configs/keybindings.el")

;; Garbage collection
(setq gc-cons-threshold (* 2 1000 1000))

;; Rainbow Delimeters colors
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 (custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
  '(rainbow-delimiters-depth-2-face ((t (:foreground "#D08770"))))
  '(rainbow-delimiters-depth-3-face ((t (:foreground "#EBCB8B"))))
  '(rainbow-delimiters-depth-4-face ((t (:foreground "#A3BE8C"))))
  '(rainbow-delimiters-depth-5-face ((t (:foreground "#88C0D0"))))
  '(rainbow-delimiters-depth-6-face ((t (:foreground "#81A1C1"))))
  '(rainbow-delimiters-depth-7-face ((t (:foreground "#5E81AC"))))))

;;; init.el ends here
