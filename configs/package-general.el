;;; package-general.el --- General Package Config

;;; Author: Javier de Marco <javierdemarcoo@gmail.com>
;;; URL: https://gitlab.com/javierdemarco
;;; Version: 0.0.1

;;; Commentary:
;;; General Package Config

;;; Code:

;;---------------------;;
;; GENERAL             ;;
;;---------------------;;

;; Rainbow Parenthesis
(use-package gnu-elpa-keyring-update)

(use-package rainbow-delimiters
  :hook (prog-mode-hook . rainbow-delimiters-mode))

;; Ivi
(use-package ivy
 :diminish
 :bind
 (:map ivy-minibuffer-map
 ("TAB" . ivy-alt-done)
 ("C-j" . ivy-next-line)
 ("C-k" . ivy-previous-line))
 (:map ivy-switch-buffer-map
 ("C-k" . ivy-previous-line)
 ("C-l" . ivy-done)
 ("C-d" . ivy-switch-buffer-kill))
 (:map ivy-reverse-i-search-map
 ("C-k" . ivy-previous-line)
 ("C-d" . ivy-reverse-i-search-kill))
 :config
 (ivy-mode 1))

;; Ivy Rich
(use-package ivy-rich
 :init
 (ivy-rich-mode 1))
;; Counsel
(use-package counsel
  :custom
  (ivy-initial-inputs-alist nil)
  (counsel-describe-function-function #'helpful-function)
  (counsel-describe-symbol-function #'helpful-symbol)
  (counsel-describe-variable-function #'helpful-variable)
  (counsel-outline-face-style 'org)
  (counsel-outline-path-separator " / ")
  :config
  (counsel-mode 1))

;; General keybindings
(use-package general)

;; Flycheck
(use-package flycheck
  :init
  (global-flycheck-mode)
  :config
  (add-hook 'after-init-hook #'global-flycheck-mode))

;; Load theme
(use-package doom-themes
  :config
  (setq doom-themes-enable-bold t
	doom-themes-enable-italic t)
  (load-theme 'doom-one t)
  (doom-themes-visual-bell-config))

;; Projectile
(use-package projectile
  :config
  ;; Use it everywhere
  (projectile-mode 1)
  :custom
  (projectile-completion-system 'ivy)
  (projectile-dynamic-mode-line nil)
  (projectile-enable-caching t)
  (projectile-track-known-projects-automatically nil))

;; Swipper
(use-package swiper
  :after ivy
  :custom
  (swiper-goto-start-of-match t))

;; Evil
(use-package evil
  :init
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1))

;; Esup
(use-package esup)

;; Counsel projectile
(use-package counsel-projectile
  :after counsel
  :config
  (counsel-projectile-mode 1))

;; Dashboard
(use-package dashboard
  :init
  (dashboard-setup-startup-hook)
  :config
  (setq
   initial-buffer-choice (lambda () (get-buffer "*dashboard*"))
   dashboard-banner-logo-title "Welcome to Zero Emacs!"
   dashboard-set-heading-icons t
   dashboard-set-file-icons t
   dashboard-set-init-info t
   dashboard-set-navigator t
   dashboard-projects-switch-function 'counsel-projectile-switch-project-by-name
   dashboard-items '((recents  . 10)
                     (bookmarks . 5)
                     (projects . 5))))

;; Highlightl TODO
(use-package hl-todo
  :init
  (global-hl-todo-mode t)
  :config
  (setq hl-todo-keyword-faces
        '(("TODO"   . "#FF0000")
          ("FIXME"  . "#FF0000")
          ("DEBUG"  . "#A020F0")
          ("GOTCHA" . "#FF4500")
          ("STUB"   . "#1E90FF"))))

;; Markdown
(use-package markdown-mode
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode)
         ("INSTALL" . markdown-mode))
  :init (setq markdown-command "multimarkdown")
  :config
  (unbind-key "<M-down>" markdown-mode-map)
  (unbind-key "<M-up>" markdown-mode-map))

;; Window movement
(use-package windmove)

;; Whitespace
(use-package whitespace
  :hook
  (prog-mode . whitespace-mode)
  (text-mode . whitespace-mode)
  :custom
  (whitespace-style '(face empty indentation::space tab trailing)))

;; All the Icons
(use-package all-the-icons)
(use-package all-the-icons-ibuffer
  :init (all-the-icons-ibuffer-mode 1))
(use-package all-the-icons-ivy
  :config
  (setq all-the-icons-ivy-file-commands
        '(counsel-find-file counsel-file-jump counsel-recentf counsel-projectile-find-file counsel-projectile-find-dir))
  (all-the-icons-ivy-setup))

;; Doom Modeline
(use-package doom-modeline
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

;; Which Key
(use-package which-key
  :init
  (which-key-mode)
  :config
  (setq which-key-idle-delay 0.3))

;;---------------------;;
;; LSP                 ;;
;;---------------------;;
(use-package company
  :config
  (company-mode)
  (add-hook 'after-init-hook 'global-company-mode))

(use-package company-box
 :hook (company-mode . company-box-mode))

(use-package lsp-mode
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "C-c l")
  :config
  (setq lsp-idle-delay 0.3
        lsp-enable-symbol-highlighting t
        lsp-enable-snippet nil  ;; Not supported by company capf, which is the recommended company backend
        )
  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
         (python-mode . lsp)
         (c-mode . lsp)
         (c++-mode . lsp)
	     (bash-mode .lsp)
         (haskell-mode-hook . lsp)
         (haskell-literate-mode-hook . lsp)
         (lsp-mode . lsp-enable-which-key-integration))
  :commands lsp)

(use-package lsp-haskell)

;; optionally
(use-package lsp-ui
  :config (setq lsp-ui-sideline-show-hover t
                lsp-ui-sideline-delay 0.5
                lsp-ui-doc-delay 5
                lsp-ui-doc-position 'bottom
                lsp-ui-doc-alignment 'frame
                lsp-ui-doc-header nil
                lsp-ui-doc-include-signature t
                lsp-ui-doc-use-childframe t)
  :commands lsp-ui-mode
  :bind (:map evil-normal-state-map
              ("gd" . lsp-ui-peek-find-definitions)
              ("gr" . lsp-ui-peek-find-references))
        (:map md/leader-map
              ("Ni" . lsp-ui-imenu)))
;; if you are ivy user
(use-package lsp-ivy :commands lsp-ivy-workspace-symbol)

;; optionally if you want to use debugger
;;(use-package dap-mode)
;; (use-package dap-LANGUAGE) to load the dap adapter for your language

;;---------------------;;
;; VERSION CONTROL     ;;
;;---------------------;;

;; Magit
(use-package magit
  :bind ("C-x g" . magit-status))
;; Git Gutter
(use-package git-gutter
  :init
  (global-git-gutter-mode t)
  :config
  (setq
   git-gutter:modified-sign "|"
   git-gutter:update-interval 2
   )
  (set-face-foreground 'git-gutter:modified "blue")
  (set-face-foreground 'git-gutter:added "green")
  (set-face-foreground 'git-gutter:deleted "red")
  :bind
  (("C-x C-g" . git-gutter)))

;;; package-general.el ends here
