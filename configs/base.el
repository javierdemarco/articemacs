;;; base.el --- Base Configuration

;;; Author: Javier de Marco <javierdemarcoo@gmail.com>
;;; URL: https://gitlab.com/javierdemarco/emacs
;;; Version: 0.0.1


;;; Commentary:
;;; Base Configuration

;;; Code:

;; Package Configuration
;; Enables packaging support
(require 'package)

;; Adds to the archive the list of available repositories
(setq package-archives '(
	    ("melpa" . "https://melpa.org/packages/")
        ("org" . "https://orgmode.org/elpa/")
        ("elpa" . "https://elpa.gnu.org/packages/")))

;; Initializes the package infrastructure
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

;; General
(save-place-mode 1)
(setq save-interprogram-paste-before-kill t
      require-final-newline t
      visible-bell t
      load-prefer-newer t
      custom-file (expand-file-name "~/.emacs.d/custom.el"))
(unless backup-directory-alist
  (setq backup-directory-alist `(("." . ,(concat user-emacs-directory
                                                  "backups")))))
(column-number-mode)
(global-display-line-numbers-mode 1)                   ; Enable Line numbers globally
(setq-default
 tab-width 4                                           ; Tab width
 delete-by-moving-to-trash t                           ; Delete to trash
 fill-column 100                                       ; Column Length
 cursor-in-non-selected-windows nil                    ; No cursor in inactive windows
 cursor-type 'block                                    ; Cursor bar
 help-window-select t                                  ; Focus new help windows
 indent-tabs-mode nil                                  ; No tabs
 initial-scratch-message ""                            ; Clean Scratch
 mouse-yank-at-point t                                 ; Yank at point rather tan pointer
 select-enable-clipboard t                             ; use system clipboard
 uniquify-buffer-name-style 'foward                    ; Uniquify buffer names
 window-combination-resize t                           ; Resize windows proportionally
 x-stretch-cursor t                                    ; Strech cursor to glyph
 display-line-numbers-current-absolute t               ; Current line is 0
 display-line-numbers-type 'relative                   ; Prefer relative numbers
 display-line-numbers-width 3                          ; Enforce width to reduce computation
 wdired-allow-to-change-permissions t                  ; Edit permissions in dired
 )
(add-to-list 'default-frame-alist '(drag-internal-border . 1))  ;; Add border to be able to resize
(add-to-list 'default-frame-alist '(internal-border-width . 2))
(global-hl-line-mode t)                              ; Hightlight line
(fset 'yes-or-no-p 'y-or-n-p)                        ; Y or N to all yes or no questions
(add-hook 'before-save-hook 'delete-trailing-whitespace) ; Delete trailing spaces
(global-auto-revert-mode t)                          ; Refresh file if changed
(set-default-coding-systems 'utf-8)                  ; Default to utf-8 encoding
(eldoc-mode t)                                       ; Eldoc
(add-hook 'comint-output-filter-functions            ; Password hidden
          'comint-watch-for-password-prompt)

;; UI Minimal
(menu-bar-mode -1)                                    ; Disable the menu bar
(scroll-bar-mode -1)                                  ; Disable Scroll bar
(tool-bar-mode -1)                                    ; Disable Tool bar
(tooltip-mode -1)                                     ; Disable Tooltips
(set-fringe-mode 10)

;; Font Config
(add-to-list 'default-frame-alist
             '(font . "FiraCode NF-15:Retina"))

;; Comment line
(defun toggle-comment-on-line ()
  "This is a function to comment a line of code."
  (interactive)
  (comment-or-uncomment-region (line-beginning-position) (line-end-position)))

;; Garbage collection make emacs snappier
(add-hook 'after-save-hook       #'garbage-collect)
;;(add-hook 'focus-out-hook        #'clean-buffer-list)
;;(add-hook 'focus-out-hook        #'garbage-collect)
;;(add-hook 'kill-buffer-hook      #'clean-buffer-list)
;;(add-hook 'kill-buffer-hook      #'garbage-collect)
;;(add-hook 'minibuffer-exit-hook  #'garbage-collect)
;;(add-hook 'minibuffer-setup-hook #'garbage-collect)
;;(add-hook 'suspend-hook          #'garbage-collect)

;; Use a hook so the message doesn't get clobbered by other messages.
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

;; No line numbers in some modes
(dolist (mode '(org-src-mode-hook
                term-mode-hook
                shell-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; PANDOC MARKDOWN
(custom-set-variables
 '(markdown-command "/usr/bin/pandoc"))

(setenv "PATH"
  (concat "~/.local/bin:"
          (getenv "PATH")))

(setenv "SHELL"
        "/bin/zsh")

;;; base.el ends here
