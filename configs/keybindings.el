;;; keybindings.el --- Keybindings Configuration

;;; Author: Javier de Marco <javierdemarcoo@gmail.com>
;;; URL: https://gitlab.com/javierdemarco
;;; Version: 0.0.1

;;; Commentary:
;;; Keybindings Configuration

;;; Variables:
(defconst zero-LEADER "SPC"
  "Leader key for keybinding.")
(defconst zero-LEADER-alt "M-SPC"
  "Alternative Leader key for keybindings.")

;;; Code:

;; GENERAL KEYS
(general-define-key
 "M-o" 'other-window            ; Go to other window
 "C-;" 'toggle-comment-on-line  ; Toggle comment in line
 "ESC" 'keyboard-escape-quit)   ; Quit command on Escape
(general-define-key
  :states '(normal visual insert emacs)
  :prefix zero-LEADER
  :non-normal-prefix zero-LEADER-alt
  "." '(counsel-find-file :which-key "Find File")
  "b" '(ibuffer :which-key "iBuffer")
  "B" '(counsel-switch-buffer :which-key "Switch Buffer")
  "x" '(counsel-M-x :which-key "Execute command")
  "s" '(swiper :which-key "Swiper")
)

;; WINDOWS KEYS
(general-define-key
  :states '(normal visual insert emacs)
  :prefix (concat zero-LEADER " w")
  :non-normal-prefix (concat zero-LEADER-alt " w")
  "w" '(delete-window :which-key "Delete Window")
  "h" '(split-window-horizontally :which-key "Split Horizontal")
  "v" '(split-window-vertically :which-key "Split Vertical")
  )
(general-define-key
  :states '(normal visual insert emacs)
  "C-k" '(windmove-up :which-key "Windmove Up")
  "C-l" '(windmove-right :which-key "Windmove Right")
  "C-j" '(windmove-down :which-key "Windmove Down")
  "C-h" '(windmove-left :which-key "Windmove Left")
  )

;; OPEN KEYS
(general-define-key
  :states '(normal visual insert emacs)
  :prefix (concat zero-LEADER " o")
  :non-normal-prefix (concat zero-LEADER-alt " o")
  "t" '(term :which-key "Terminal")
  "e" '(eshell :which-key "EShell")
  "d" '(counsel-dired :which-key "Dired")
  )

;; BUFFER/FILE KEYS
(general-define-key
  :states '(normal visual insert emacs)
  :prefix (concat zero-LEADER " f")
  :non-normal-prefix (concat zero-LEADER-alt " f")
  "s" '(save-buffer :which-key "Save Buffer")
  "m" '(mark-whole-buffer :whick-key "Mark Whole Buffer")
  "g" '(goto-line :which-key "Go to Line")
  "r" '(counsel-recentf :which-key "Open Recent File")
  )

;; HELP KEYS
(general-define-key
  :states '(normal visual insert emacs)
  :prefix (concat zero-LEADER " h")
  :non-normal-prefix (concat zero-LEADER-alt " h")
  "f" '(counsel-describe-function :which-key "Describe Function")
  "v" '(counsel-describe-variable :which-key "Describe Variable")
  "o" '(counsel-describe-symbol which-key "Describe Symbol")
  "b" '(counsel-descbinds :which-key "Describe Keybindings")
  )

;; PROJECTILE KEYS
(general-define-key
  :states '(normal visual insert emacs)
  :prefix (concat zero-LEADER " p")
  :non-normal-prefix (concat zero-LEADER-alt " p")
  "f" '(projectile-find-file :which-key "Projectile Find File")
  "p" '(counsel-describe-variable :which-key "Projectile Command Map")
  )

;;; keybindings.el ends here
